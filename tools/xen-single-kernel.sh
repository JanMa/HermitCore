#!/bin/bash
# ------------------------------------------------------------------
# [Jan Martens] Xen Single Kernel HVM Boot
#          Boots a Hermitcore application as a single kernel HVM Domain in Xen
# ------------------------------------------------------------------

SUBJECT=xen-single-kernel
VERSION=0.1.0
USAGE="Usage: command -hvlknmc args

This script starts a HermitCore application as a single kernel HVM Domain in Xen.
Make sure to destroy your domain when you are finished!

Args:
  -l  Path to hermit loader
  -k  Path to hermit application
  -n  Only create files, do not start
  -m  Domain memory in MB (Default 1024)
  -c  Number of CPU cores (Default 1)
  -v  print VERSION
  -h  this help screen"
#----- Set defaults ------------------------------------------------
CPU="1"
MEMORY="1024"
DRY=""
KERNEL=""
LOADER=""
# --- Option processing --------------------------------------------
if [ $# == 0 ] ; then
    echo "$USAGE"
    exit 1;
fi

while getopts ":vhl:k:nm:c:" optname
  do
    case "$optname" in
      "v")
        echo "Version $VERSION"
        exit 0;
        ;;
      "h")
        echo "$USAGE"
        exit 0;
        ;;
      "?")
        echo "Unknown option $OPTARG"
        exit 0;
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        exit 0;
        ;;
      "k")
        echo "Using kernel $OPTARG"
        KERNEL="$OPTARG"
        ;;
      "l")
        echo "Using loader $OPTARG"
        LOADER="$OPTARG"
        ;;
      "n")
        echo "Dry run! Create all files but do not start the domain"
        DRY="1"
        ;;
      "m")
        echo "Allocating $OPTARG MB for the new domain"
        MEMORY="$OPTARG"
        ;;
      "c")
        echo "Allocating $OPTARG CPU cores for the new domain"
        CPU="$OPTARG"
        ;;
      *)
        echo "Unknown error while processing options"
        exit 0;
        ;;
    esac
  done

shift $(($OPTIND - 1))

# -----------------------------------------------------------------

LOCK_FILE=/tmp/${SUBJECT}.lock

if [ -f "$LOCK_FILE" ]; then
echo "Script is already running"
exit
fi

# -----------------------------------------------------------------
trap "rm -f $LOCK_FILE" EXIT
touch $LOCK_FILE

# -----------------------------------------------------------------
#  SCRIPT LOGIC GOES HERE
# -----------------------------------------------------------------
#cheking dependencies
hash xl 2>/dev/null || { echo >&2 "xl is required but not installed. Aborting!"; exit 1; }
hash grub-mkrescue 2>/dev/null || { echo >&2 "grub-mkrescue is required but not installed. Aborting!"; exit 1; }
#check for valid kernel and loader
if [[ ! -a $KERNEL ]]; then
  echo "Missing valid kernel. Aborting!"
  exit 1
fi
if [[ ! -a $LOADER ]]; then
  echo "Missing valid loader. Aborting!"
  exit 1
fi
echo "Configuring new domain..."

#delete old iso if present
if [[ -a "/tmp/hermit.iso" ]]; then
  rm /tmp/hermit.iso
fi
#create neccesary iso image
mkdir -p /tmp/iso/boot/grub
cat <<EOF > /tmp/iso/boot/grub/grub.cfg
default=0
timeout=0

menuentry "HermitCore" {
	multiboot /boot/ldhermit.elf -uart=io:0x3f8
	module /boot/hermit
	boot
}
EOF
if [[ ! -a "/tmp/iso/boot/grub/grub.cfg" ]]; then
  echo "could not create grub.cfg. Aborting!"
  exit 1
fi
cp $KERNEL /tmp/iso/boot/hermit
cp $LOADER /tmp/iso/boot/ldhermit.elf

grub-mkrescue -o /tmp/hermit.iso /tmp/iso 2>	/dev/null
if [[ -a "/tmp/hermit.iso" ]]; then
  rm -rf /tmp/iso/
else
  rm -rf /tmp/iso/
  echo "Could not create hermit.iso. Aborting!"
  exit 1
fi

#check if we have enough memory
FREEMEM="$(xl info | grep free_memory | sed -e 's/.*: //g')"
if [[ "$MEMORY" -gt "$FREEMEM" ]]; then
  echo "Requesting too much Memory. Aborting!"
  exit 1
fi
#check if we have enough cpu cores
FREECPU="$(xl info | grep nr_cpus | sed -e 's/.*: //g')"
if [[ "$CPU" -gt "$FREECPU" ]]; then
  echo "Requesting too many CPU cores. Aborting!"
  exit 1
fi

#create domain config
cat <<EOF > /tmp/domain_config
# This configures an HVM rather than PV guest
builder = "hvm"

# Guest name
name = "hermit-single.hvm"

# Initial memory allocation (MB)
memory = $MEMORY

# Number of VCPUS
vcpus = $CPU

# Network devices
vif = [ 'model=rtl8139,bridge=br0' ]

# Disk Devices
disk = [ 'file:/tmp/hermit.iso,hdc:cdrom,r' ]

#Disable VGA output
nographic = 1

#Serial Console Output
serial = [ 'file:/tmp/hermit.log' ]

#Set tsc mode to native
tsc_mode="native"
EOF

if [[ ! -a "/tmp/domain_config" ]]; then
  echo "Could not create domain_config. Aborting!"
  exit 1
fi
#delete old logs if present
if [[ -a "/tmp/hermit.log" ]]; then
  rm /tmp/hermit.log
fi
touch /tmp/hermit.log
if [[ -z $DRY ]]; then
  echo "Starting new domain..."
  #start domain
  xl -v create /tmp/domain_config
  #attach output
  tail -f /tmp/hermit.log
fi
