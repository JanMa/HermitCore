#!/bin/bash
IMAGE="rwthos/hermitcore:xen"
NAME="hermit_build"

echo "Delete old container (if any)"
docker ps -a | grep -q hermit_build
if [[ "$?" -eq "0" ]]; then
  docker stop hermit_build
  docker rm hermit_build
fi
echo "Start new container"
docker run -dit --name ${NAME} -v "$(pwd)/..":/HermitCore ${IMAGE}
echo "Build HermitCore"
docker exec ${NAME} /HermitCore/docker/docker-build-hermit.sh
sudo chown -R "$(id -un)":"$(id -gn)" "$(pwd)/../build"
echo "Done!"
