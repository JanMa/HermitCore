## xen config options ##

### options ###

- vga="STRING"
  - Selects the emulated video card (none|stdvga|cirrus|qxl). The default is cirrus.
    In general, QXL should work with the Spice remote display protocol for acceleration, and QXL driver is necessary in guest in this case. QXL can also work with the VNC protocol, but it will be like a standard VGA without acceleration.
- vcpus=N
  - Start the guest with N vcpus initially online.
- memory=MBYTES
  - Start the guest with MBYTES megabytes of RAM.
- vif=[ "NET_SPEC_STRING", "NET_SPEC_STRING", ...]
  - Specifies the networking provision (both emulated network adapters, and Xen virtual interfaces) to provided to the guest. See docs/misc/xl-network-configuration.markdown.
        type:
        This keyword is valid for HVM guests only.

        Specifies the type of device to valid values are:

          ioemu (default) -- this device will be provided as an emulate device to the guest and also as a paravirtualised device which the guest may choose to use instead if it has suitable drivers available.
          vif -- this device will be provided as a paravirtualised device only.

        model:
        This keyword is valid for HVM guest devices with type=ioemu only.

        Specifies the type device to emulated for this guest. Valid values are:

          rtl8139 (default) -- Realtek RTL8139
          e1000 -- Intel E1000
          in principle any device supported by your device model
- kernel="PATHNAME"
  - Load the specified file as the kernel image.
- ramdisk="PATHNAME"
  - Load the specified file as the ramdisk.
- extra="STRING"
  - Append STRING to the kernel command line. (Note: it is guest specific what meaning this has).
