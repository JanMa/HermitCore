#include <xen/console.h>
#include <xen/sched.h>
//#include <xen/barrier.h>
#include <xen/event.h>
#include <hermit/stdio.h>
#include <string.h>
#include <xen/hypervisor.h>
#include <asm/page.h>

static evtchn_port_t console_evt;
extern const void kernel_start;
struct xencons_interface * console;

/* Initialise the console */
int console_init(void)
{
	//console = (struct xencons_interface*)
	//	((machine_to_phys_mapping[start->console.domU.mfn] << 12));
	//console_evt = start->console.domU.evtchn;
	/* Set up the event channel */
	//register_event(console_evt, handle_input);
	//return 0;
	uint64_t v = -1;

    if (hvm_get_parameter(HVM_PARAM_CONSOLE_EVTCHN, &v)){
        asm volatile ("hlt");
	}
    xprintk("Console event channel: %d\n", v);
	console_evt = v;

    if (hvm_get_parameter(HVM_PARAM_CONSOLE_PFN, &v)){
    asm volatile ("hlt");
	}

	xprintk("Console page: 0x%lx\n", v);
	xprintk("Console address: 0x%lx\n", pfn_to_virt(v));
	int ret = page_map(pfn_to_virt(v), pfn_to_virt(v), 1, PG_GLOBAL|PG_RW);
	if (ret){
		xprintk("Mapping console page failed. RC: %d", ret);
		return -1;
	}
	xprintk("Mapped Console page. RC %d\n", ret);
	console = (struct xencons_interface *)pfn_to_virt(v);
	return 0;
}

/* Write a NULL-terminated string */
int console_write(char * message)
{
	struct evtchn_send event;
	event.port = console_evt;
	int length = 0;
	while(*message != '\0')
	{
		/* Wait for the back end to clear enough space in the buffer */
		XENCONS_RING_IDX data;
		do
		{
			data = console->out_prod - console->out_cons;
			HYPERVISOR_event_channel_op(EVTCHNOP_send, &event);
			mb();
		} while (data >= sizeof(console->out));
		/* Copy the byte */
		int ring_index = MASK_XENCONS_IDX(console->out_prod, console->out);
		console->out[ring_index] = *message;
		/* Ensure that the data really is in the ring before continuing */
		wmb();
		/* Increment input and output pointers */
		console->out_prod++;
		length++;
		message++;
	}
	HYPERVISOR_event_channel_op(EVTCHNOP_send, &event);
	return length;
}


/* Block while data is in the out buffer */
void console_flush(void)
{
	/* While there is data in the out channel */
	while(console->out_cons < console->out_prod)
	{
		/* Let other processes run */
		HYPERVISOR_sched_op(SCHEDOP_yield, 0);
		mb();
	}
}

void print(int direct, const char *fmt, va_list args)
{
    static char   buf[1024];

    (void)xsnprintf(buf, sizeof(buf), fmt, args);

    if(direct)
    {
        (void)HYPERVISOR_console_io(CONSOLEIO_write, strlen(buf), buf);
        return;
    }else{
				(void)console_write(buf);
				if (buf[strlen(buf)-1] == '\n') {
					/* Feed a carriage return after a new line,
					 * otherwise we will just start somewhere in the line
					 */
					(void)console_write("\r");
				}
				return;
		}
}

void printk(const char *fmt, ...)
{
    va_list       args;
    va_start(args, fmt);
    print(0, fmt, args);
    va_end(args);
}

void xprintk(const char *fmt, ...)
{
    va_list       args;
    va_start(args, fmt);
    print(1, fmt, args);
    va_end(args);
}
