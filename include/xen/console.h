#include <stddef.h>
#include <x86_64/hypercall-x86_64.h>
#include <xen/xen.h>
#include <xen/io/console.h>
#include <string.h>
#include <xen/hvm/params.h>

int console_write(char * message);
void console_flush(void);
void printk(const char *fmt, ...);
void xprintk(const char *fmt, ...);
