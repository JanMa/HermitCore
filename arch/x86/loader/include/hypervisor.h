/******************************************************************************
 * hypervisor.h
 * 
 * Hypervisor handling.
 * 
 *
 * Copyright (c) 2002, K A Fraser
 * Copyright (c) 2005, Grzegorz Milos
 * Updates: Aravindh Puthiyaparambil <aravindh.puthiyaparambil@unisys.com>
 *          Jan Martens <jan.martens@rwth-aachen.de>
 */

#ifndef _HYPERVISOR_H_
#define _HYPERVISOR_H_

#include <stddef.h>
#include <xen/xen.h>
#if defined(__i386__)
#include <x86_32/hypercall-x86_32.h>
#elif defined(__x86_64__)
#include <x86_64/hypercall-x86_64.h>
#else
#error "Unsupported architecture"
#endif
#include <xen/hvm/hvm_op.h>
#include <asm/x86_mm.h>

/* hypervisor.c */
int hvm_get_parameter(int idx, uint64_t *value);
int hvm_set_parameter(int idx, uint64_t value);
shared_info_t *map_shared_info(void *p);
void unmap_shared_info(void);
// void mask_evtchn(uint32_t port);
// void unmask_evtchn(uint32_t port);
// void clear_evtchn(uint32_t port);

extern int in_callback;

#endif /* __HYPERVISOR_H__ */
