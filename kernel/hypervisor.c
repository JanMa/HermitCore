/******************************************************************************
 * hypervisor.c
 * 
 * Communication to/from hypervisor.
 * 
 * Copyright (c) 2002-2003, K A Fraser
 * Copyright (c) 2005, Grzegorz Milos, gm281@cam.ac.uk,Intel Research Cambridge
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */

#include <xen/hypervisor.h>
#include <xen/memory.h>
#include <stddef.h>
#include <asm/page.h>

#define active_evtchns(cpu,sh,idx)              \
    ((sh)->evtchn_pending[idx] &                \
     ~(sh)->evtchn_mask[idx])

int in_callback;

extern shared_info_t shared_info;

int hvm_get_parameter(int idx, uint64_t *value)
{
    struct xen_hvm_param xhv;
    int ret;

    xhv.domid = DOMID_SELF;
    xhv.index = idx;
    ret = HYPERVISOR_hvm_op(HVMOP_get_param, &xhv);
    if ( ret < 0 )
        asm volatile ("hlt");

    *value = xhv.value;
    return ret;
}

int hvm_set_parameter(int idx, uint64_t value)
{
    struct xen_hvm_param xhv;

    xhv.domid = DOMID_SELF;
    xhv.index = idx;
    xhv.value = value;
    return HYPERVISOR_hvm_op(HVMOP_set_param, &xhv);
}

shared_info_t *map_shared_info(void *p)
{
    struct xen_add_to_physmap xatp;

    xatp.domid = DOMID_SELF;
    xatp.idx = 0;
    xatp.space = XENMAPSPACE_shared_info;
    xatp.gpfn = PFN_DOWN(virt_to_phys((size_t)&shared_info));
    if ( HYPERVISOR_memory_op(XENMEM_add_to_physmap, &xatp) != 0 )
        asm volatile ("hlt");

    return &shared_info;
}

void unmap_shared_info(void)
{
    struct xen_remove_from_physmap xrtp;

    xrtp.domid = DOMID_SELF;
    xrtp.gpfn = PFN_DOWN(virt_to_phys((size_t)&shared_info));
    if ( HYPERVISOR_memory_op(XENMEM_remove_from_physmap, &xrtp) != 0 )
        asm volatile ("hlt");

    return;
}
