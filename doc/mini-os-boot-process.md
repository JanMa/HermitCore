# Mini-Os Boot Process
Mini-Os boots at the symbol " \_start " in "arch/x86/x86_64.S"
It starts by allocating the stack size on the stack pointer and then calls "arch_init" in "setup.c"
```
cld
movq stack_start(%rip),%rsp
andq $(~(__STACK_SIZE-1)), %rsp
movq %rsi,%rdi
call arch_init
```
"arch_init" then initializes a bunch of architectural stuff before it calls the "start_kernel" function.
```
arch_init(void *par)
{
	static char hello[] = "Bootstrapping...\n";

	hpc_init();
	(void)HYPERVISOR_console_io(CONSOLEIO_write, strlen(hello), hello);

	trap_init();

	/*Initialize floating point unit */
	fpu_init();

	/* Initialize SSE */
	sse_init();

	/* Setup memory management info from start_info. */
	arch_mm_preinit(par);

	/* WARN: don't do printk before here, it uses information from
	   shared_info. Use xprintk instead. */
	get_console(par);
	get_xenbus(par);
	get_cmdline(par);

	/* Grab the shared_info pointer and put it in a safe place. */
	HYPERVISOR_shared_info = map_shared_info(par);

	/* print out some useful information  */
	print_start_of_day(par);

#ifdef CONFIG_PARAVIRT
	memcpy(&start_info, par, sizeof(start_info));
#endif
	start_info_ptr = (start_info_t *)par;

	start_kernel((start_info_t *)par);
}
```
"start_kernel" accepts a "start_info_t" structure made available by the xen domain creator (xl). The definition of this structure is in "include/xen.h".
```
/*
 * `incontents 200 startofday Start-of-day memory layout
 *
 *  1. The domain is started within contiguous virtual-memory region.
 *  2. The contiguous region ends on an aligned 4MB boundary.
 *  3. This the order of bootstrap elements in the initial virtual region:
 *      a. relocated kernel image
 *      b. initial ram disk              [mod_start, mod_len]
 *         (may be omitted)
 *      c. list of allocated page frames [mfn_list, nr_pages]
 *         (unless relocated due to XEN_ELFNOTE_INIT_P2M)
 *      d. start_info_t structure        [register rSI (x86)]
 *         in case of dom0 this page contains the console info, too
 *      e. unless dom0: xenstore ring page
 *      f. unless dom0: console ring page
 *      g. bootstrap page tables         [pt_base and CR3 (x86)]
 *      h. bootstrap stack               [register ESP (x86)]
 *  4. Bootstrap elements are packed together, but each is 4kB-aligned.
 *  5. The list of page frames forms a contiguous 'pseudo-physical' memory
 *     layout for the domain. In particular, the bootstrap virtual-memory
 *     region is a 1:1 mapping to the first section of the pseudo-physical map.
 *  6. All bootstrap elements are mapped read-writable for the guest OS. The
 *     only exception is the bootstrap page table, which is mapped read-only.
 *  7. There is guaranteed to be at least 512kB padding after the final
 *     bootstrap element. If necessary, the bootstrap virtual region is
 *     extended by an extra 4MB to ensure this.
 *
 * Note: Prior to 25833:bb85bbccb1c9. ("x86/32-on-64 adjust Dom0 initial page
 * table layout") a bug caused the pt_base (3.g above) and cr3 to not point
 * to the start of the guest page tables (it was offset by two pages).
 * This only manifested itself on 32-on-64 dom0 kernels and not 32-on-64 domU
 * or 64-bit kernels of any colour. The page tables for a 32-on-64 dom0 got
 * allocated in the order: 'first L1','first L2', 'first L3', so the offset
 * to the page table base is by two pages back. The initial domain if it is
 * 32-bit and runs under a 64-bit hypervisor should _NOT_ use two of the
 * pages preceding pt_base and mark them as reserved/unused.
 */
#ifdef XEN_HAVE_PV_GUEST_ENTRY
struct start_info {
    /* THE FOLLOWING ARE FILLED IN BOTH ON INITIAL BOOT AND ON RESUME.    */
    char magic[32];             /* "xen-<version>-<platform>".            */
    unsigned long nr_pages;     /* Total pages allocated to this domain.  */
    unsigned long shared_info;  /* MACHINE address of shared info struct. */
    uint32_t flags;             /* SIF_xxx flags.                         */
    xen_pfn_t store_mfn;        /* MACHINE page number of shared page.    */
    uint32_t store_evtchn;      /* Event channel for store communication. */
    union {
        struct {
            xen_pfn_t mfn;      /* MACHINE page number of console page.   */
            uint32_t  evtchn;   /* Event channel for console page.        */
        } domU;
        struct {
            uint32_t info_off;  /* Offset of console_info struct.         */
            uint32_t info_size; /* Size of console_info struct from start.*/
        } dom0;
    } console;
    /* THE FOLLOWING ARE ONLY FILLED IN ON INITIAL BOOT (NOT RESUME).     */
    unsigned long pt_base;      /* VIRTUAL address of page directory.     */
    unsigned long nr_pt_frames; /* Number of bootstrap p.t. frames.       */
    unsigned long mfn_list;     /* VIRTUAL address of page-frame list.    */
    unsigned long mod_start;    /* VIRTUAL address of pre-loaded module   */
                                /* (PFN of pre-loaded module if           */
                                /*  SIF_MOD_START_PFN set in flags).      */
    unsigned long mod_len;      /* Size (bytes) of pre-loaded module.     */
#define MAX_GUEST_CMDLINE 1024
    int8_t cmd_line[MAX_GUEST_CMDLINE];
    /* The pfn range here covers both page table and p->m table frames.   */
    unsigned long first_p2m_pfn;/* 1st pfn forming initial P->M table.    */
    unsigned long nr_p2m_frames;/* # of pfns forming initial P->M table.  */
};
typedef struct start_info start_info_t;
```
"start_kernel" is defined in "kernel.c". It sets up events,  xen_features, memory management, time, console, the schedulder and the xenbus interface. Then it calls the actual main function "app_main".
```
void start_kernel(void* par)
{
    printk("*** Enter start_kernel(void* par) ***\n");
    /* Set up events. */
    init_events();

    /* ENABLE EVENT DELIVERY. This is disabled at start of day. */
    local_irq_enable();

    setup_xen_features();

    /* Init memory management. */
    init_mm();

    /* Init time and timers. */
    init_time();

    /* Init the console driver. */
    init_console();

    /* Init grant tables */
    init_gnttab();

    /* Init scheduler. */
    init_sched();

    /* Init XenBus */
    init_xenbus();

#ifdef CONFIG_XENBUS
    /* Init shutdown thread */
    init_shutdown((start_info_t *)par);
#endif

    /* Call (possibly overridden) app_main() */
    app_main(NULL);

    /* Everything initialised, start idle thread */
    run_idle_thread();
}
```
