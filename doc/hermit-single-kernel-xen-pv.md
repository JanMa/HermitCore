# Make HermitCore boot in Xen (Single Kernel PV)

[1]: https://www.gnu.org/software/grub/manual/multiboot/multiboot.html
[2]: https://wiki.xenproject.org/wiki/PyGrub
[3]: https://wiki.xen.org/wiki/PvGrub2
[4]: https://github.com/xen-project/xen/blob/master/docs/misc/x86-xenpv-bootloader.markdown
[5]: https://wiki.xen.org/wiki/X86_Paravirtualised_Memory_Management
[6]: https://www.netbsd.org/docs/kernel/elf-notes.html
[7]: https://xenbits.xen.org/gitweb/?p=mini-os.git;a=blob_plain;f=include/xen/elfnote.h;h=936aa658222d38804985ebf8787a3b5db3c30327;hb=HEAD
[8]: https://www.ibm.com/developerworks/library/l-gas-nasm/

HermitCore is a GRUB Mutliboot comaptible kernel [\[1\]][1]. It includes all the necessary information to be (in theory) booted by a multiboot compatible bootloader.

```bash
[jan@Jan-PC hermit]$ grub-file --is-x86-multiboot bin/ldhermit.elf
[jan@Jan-PC hermit]$ if [[ !("${?}") ]]; then ( echo "Yes" ) else (echo "No") fi
Yes
```

Xen includes two possibilities to to boot a kernel via multiboot. You can either boot it directly via the integrated multiboot-bootloader or via the pygrub/pvgrub bootloader which is fully compatible to the GRUB2 bootloader. [\[2\]][2][\[3\]][3]
It can also boot ELF binarys directly.

We shall try to implement the necessary changes to use the included generic ELF loader. We want to get HermitCore up and running as a paravirtualized guest in Xen. Since Xen boots paravirtualized guests directly into 64 Bit mode, we don't have to implement any changes in the `ldhermit.elf` loader binary. We have to make some changes to it when we want to boot in a fully hardware virtualized machine in Xen.

First we need to include the standard Xen ELF notes allowing the binary to be loaded by the xen toolstack domain builder. Let's have a look at their definition and how they are implemented in Mini-OS. Then we port the implementation to HermitCore.

Before the guest is started, the kernel image is read and the `ELF PT_NOTE` program header is parsed. The hypervisor looks in the `.note` sections for the `Xen` notes. The description fields are Xen specific and contain the required information to find out: where the kernel expects its virtual base address, what type of hypervisor it can work with, certain features the kernel image can support, and the location of the hypercall page, etc. [\[5\]][5] All ELF Note elements have the same basic structure:

```
Name Size
4 bytes (integer)
Desc Size
4 bytes (integer)
Type
4 bytes (usually interpreted as an integer)
Name
variable size, padded to a 4 byte boundary
Desc
variable size, padded to a 4 byte boundary
```

The `Name Size` and `Desc Size` fields are integers (in the byte order specified by the binary's ELF header) which specify the size of the `Name` and `Desc` fields (excluding padding).
The `Name` field specifies the vendor who defined the format of the Note. Typically, vendors use names which are related to their project and/or company names. For instance, the GNU Project uses `GNU` as its name. No two vendors should use the same ELF Note Name, lest there be confusion when trying to interpret the meanings of notes.
The `Type` field is vendor specific, but it is usually treated as an integer which identifies the type of the note.
The `Desc` field is vendor specific, and usually contains data which depends on the note type. [\[6\]][6]

Xen defines the following types for ELF notes:

```
XEN_ELFNOTE_INFO
XEN_ELFNOTE_ENTRY
XEN_ELFNOTE_HYPERCALL_PAGE
XEN_ELFNOTE_VIRT_BASE
XEN_ELFNOTE_PADDR_OFFSET
XEN_ELFNOTE_XEN_VERSION
XEN_ELFNOTE_GUEST_OS
XEN_ELFNOTE_GUEST_VERSION
XEN_ELFNOTE_LOADER
XEN_ELFNOTE_PAE_MODE
XEN_ELFNOTE_FEATURES
XEN_ELFNOTE_BSD_SYMTAB
XEN_ELFNOTE_HV_START_LOW
XEN_ELFNOTE_L1_MFN_VALID
XEN_ELFNOTE_SUSPEND_CANCEL
XEN_ELFNOTE_INIT_P2M
XEN_ELFNOTE_MOD_START_PFN
XEN_ELFNOTE_SUPPORTED_FEATURES
XEN_ELFNOTE_PHYS32_ENTRY
```

For a description of the different types and their descriptions have a look at [`elfnotes.h`][7] in Mini-OS. Mini-OS implements a macro to add ELF notes in the following way.

```
#define ELFNOTE(name, type, desc)           \
    .pushsection .note.name               ; \
    .align 4                              ; \
    .long 2f - 1f       /* namesz */      ; \
    .long 4f - 3f       /* descsz */      ; \
    .long type          /* type   */      ; \
1:.asciz #name          /* name   */      ; \
2:.align 4                                ; \
3:desc                  /* desc   */      ; \
4:.align 4                                ; \
    .popsection
```
It can be called like this
```
ELFNOTE(Xen, XEN_ELFNOTE_GUEST_OS, .asciz "Mini-OS-x86_64")
ELFNOTE(Xen, XEN_ELFNOTE_LOADER, .asciz "generic")
```

Unfortunately Mini-OS compiles it's assembly files with `gcc` which uses the AT&T assembly syntax. HermitCore on the other hand uses `nasm` which uses the Intel assembly syntax. Since they are not compatible with each other we have to rewrite the `ELFNOTE` macro. [\[8\]][8] has a very helpful comparison of the two styles. Without goining into the details, here is an equivalent implementation in Intel assembly syntax for HermitCore:

```nasm
%macro elfnote 2-3 ""  ;name, type, descr

  align 4
  %strlen namesz %1
  %strlen descsz %3
  dd namesz
  dd descsz
  dd %2
  %if descsz > 0
  dd %1
    dd %3
  %endif

%endmacro
```
It can be called like this:
```nasm
SECTION .note
elf_notes:
  elfnote "Xen",XEN_ELFNOTE_GUEST_OS,"HermitCore"
  elfnote "Xen",XEN_ELFNOTE_GUEST_VERSION,"0.2.5"
  elfnote "Xen",XEN_ELFNOTE_LOADER,"generic"
  elfnote "Xen",XEN_ELFNOTE_XEN_VERSION,"xen-3.0"
```

The added notes have to be linked into a seperate `.note` section (see [`link.ld`](../arch/x86/loader/link.ld) ).
```text
SECTIONS
{
  ...

  .note : {
    /*add elf notes for Xen*/
    *(.note)
    *(.note.*)
  }

  ...
}
```
With these notes added to the kernel [`entry.asm`](../arch/x86/kernel/entry.asm) file the Xen domain creator is able to detect our binary and tries to boot it.

```text
Parsing config from xlexample.pvlinux
...
domainbuilder: detail: xc_dom_parse_image: called
domainbuilder: detail: xc_dom_find_loader: trying multiboot-binary loader ...
domainbuilder: detail: loader probe failed
domainbuilder: detail: xc_dom_find_loader: trying HVM-generic loader ...
domainbuilder: detail: loader probe failed
domainbuilder: detail: xc_dom_find_loader: trying Linux bzImage loader ...
domainbuilder: detail: xc_dom_probe_bzimage_kernel: kernel is not a bzImage
domainbuilder: detail: loader probe failed
domainbuilder: detail: xc_dom_find_loader: trying ELF-generic loader ...
domainbuilder: detail: loader probe OK
xc: detail: ELF: phdr: paddr=0x800000 memsz=0x1489b168
xc: detail: ELF: memory: 0x800000 -> 0x1509b168
xc: detail: ELF: note: GUEST_OS = "HermitCore"
xc: detail: ELF: note: GUEST_VERSION = "0.2.5"
xc: detail: ELF: note: LOADER = "generic"
xc: detail: ELF: note: XEN_VERSION = "xen-3.0"
xc: detail: ELF: VIRT_BASE unset, using 0
xc: detail: ELF_PADDR_OFFSET unset, using 0
xc: detail: ELF: addresses:
xc: detail:     virt_base        = 0x0
xc: detail:     elf_paddr_offset = 0x0
xc: detail:     virt_offset      = 0x0
xc: detail:     virt_kstart      = 0x800000
xc: detail:     virt_kend        = 0x1509b168
xc: detail:     virt_entry       = 0x800000
xc: detail:     p2m_base         = 0xffffffffffffffff
...
domainbuilder: detail: xc_dom_release: called

```
Sadly our domain dies almost instantly when trying to boot it. So we have to implement some more stuff ;-)

For the next steps we shall have a look which notes Mini-OS also passes to the domain builder and which we could use as well. Then we must have a look at the memory management and layout and maybe how we could print stuff to Xen's console to ease debugging.
