## qemu startup command ##
```
qemu-system-x86_64 -daemonize -display none -smp 1 -m 2G -pidfile /tmp/hpid-5b4vcc -net nic,model=rtl8139 -net user,hostfwd=tcp:127.0.0.1:18766-:18766 -chardev file,id=gnc0,path=/tmp/hermit-VYh9uO -device pci-serial,chardev=gnc0 -kernel /.../HermitCore/build/local_prefix/opt/hermit/bin/ldhermit.elf -initrd x86_64-hermit/extra/tests/hello -append "-freq3500 -proxy"
```

### options ###
- daemonize
  - Daemonize the QEMU process after initialization.  QEMU will not detach from standard IO until it is ready to receive connections on any of its devices.  This option is a useful way for external programs to launch QEMU without having to cope with initialization race conditions.
- display none
  - Do not display video output. The guest will still see an emulated graphics card, but its output will not be displayed to the QEMU user. This option differs from the -nographic option in that it only affects what is done with video output; -nographic also changes the destination of the serial and parallel port data.
- smp 1
  - Simulate an SMP system with n CPUs. On the PC target, up to 255 CPUs are  supported. On Sparc32 target, Linux limits the number of usable CPUs to 4.  For the PC target, the number of cores per socket, the number of threads per cores and the total number of sockets can be specified. Missing values will be computed. If any on the three values is given, the total number of CPUs n can be omitted. maxcpus specifies the maximum number of hotpluggable CPUs.
- m 2G
  - Sets guest startup RAM size to megs megabytes. Default is 128 MiB.  Optionally, a suffix of "M" or "G" can be used to signify a value in megabytes or gigabytes respectively. Optional pair slots, maxmem could be used to set amount of hotpluggable memory slots and maximum amount of memory. Note that maxmem must be aligned to the page size. For example, the following command-line sets the guest startup RAM size to 1GB, creates 3 slots to hotplug additional memory and sets the maximum memory the guest can reach to 4GB:
                   qemu-system-x86_64 -m 1G,slots=3,maxmem=4G
  If slots and maxmem are not specified, memory hotplug won't be enabled and the guest startup RAM will never increase.
- pidfile
  - Store the QEMU process PID in file. It is useful if you launch QEMU from a script.
- net nic,model=rtl8139
  - Create a new Network Interface Card and connect it to VLAN n (n = 0 is the default). The NIC is an e1000 by default on the PC target. Optionally, the MAC address can be changed to mac, the device address set to addr (PCI cards only), and a name can be assigned for use in monitor commands.  Optionally, for PCI cards, you can specify the number v of MSI-X vectors that the card should have; this option currently only affects virtio cards; set v = 0 to disable MSI-X. If no -net option is specified, a single NIC is created.  QEMU can emulate several different models of network card.  Valid values for type are "virtio", "i82551", "i82557b", "i82559er", "ne2k_pci", "ne2k_isa", "pcnet", "rtl8139", "e1000", "smc91c111", "lance" and "mcf_fec".  Not all devices are supported on all targets.  Use "-net nic,model=help" for a list of available devices for your target.
- net user,hostfwd=tcp:127.0.0.1:18766-:18766
  - Use the user mode network stack which requires no administrator privilege to run. Valid options are:
        hostfwd=[tcp|udp]:[hostaddr]:hostport-[guestaddr]:guestport
        Redirect incoming TCP or UDP connections to the host port hostport to the guest IP
        address guestaddr on guest port guestport. If guestaddr is not specified, its value is
        x.x.x.15 (default first address given by the built-in DHCP server). By specifying
        hostaddr, the rule can be bound to a specific host interface. If no connection type is
        set, TCP is used. This option can be given multiple times.

        For example, to redirect host X11 connection from screen 1 to guest screen 0, use the
        following:

             # on the host
             qemu-system-i386 -net user,hostfwd=tcp:127.0.0.1:6001-:6000 [...]
             # this host xterm should open in the guest X11 server
             xterm -display :1

        To redirect telnet connections from host port 5555 to telnet port on the guest, use the
        following:

             # on the host
             qemu-system-i386 -net user,hostfwd=tcp::5555-:23 [...]
             telnet localhost 5555

        Then when you use on the host "telnet localhost 5555", you connect to the guest telnet
        server.
- chardev file,id=gnc0,path=/tmp/hermit-VYh9uO
  - Log all traffic received from the guest to a file. path specifies the path of the file to be opened. This file will be created if it does not already exist, and overwritten if it does. path is required.
- device pci-serial,chardev=gnc0
  - Add device driver.  prop=value sets driver properties.  Valid properties depend on the driver.  To get help on possible drivers and properties, use "-device help" and "-device       driver,help".
        pci-serial.rombar=uint32
        pci-serial.multifunction=bool (on/off)
        pci-serial.romfile=str
        pci-serial.prog_if=uint8
        pci-serial.command_serr_enable=bool (on/off)
        pci-serial.addr=int32 (Slot and optional function number, example: 06.0 or 06)
        pci-serial.chardev=str (ID of a chardev to use as a backend)
- kernel
  - Use bzImage as kernel image. The kernel can be either a Linux kernel or in multiboot format.
- initrd
  - Use file as initial ram disk.
- append "-freq3500 -proxy"
  - Use cmdline as kernel command line
