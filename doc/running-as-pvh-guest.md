## What would be needed to run HermitCore as a PVH guest ?

When we want to run HermitCore as a PVH guest instead of a PV guest, we have to take care of the following things:

- We need a 32 Bit entrypoint
- We have to use HVM type Hypercalls

These are the main requirements and they have some pros and cons.

Pros:
- No need for QEMU
- No need to use PV MMU and related Hypercalls
    - We are able to use hardware virtualized page tables
    - No need to change memory management code!
- Overall Speed compared to pure PV and HVM should improve

Cons:
- Have to rewrite / delete a lot of the code already implemented for a PV guest
    - A lot of work has to be done again ( but this time maybe quicker)
- Need hardware virtualization support on our Hypervisor
    - Processor has to have Intel VT-x oder AMD Hyper..? capabilities
- Need to use a very recent version of Xen
    - 4.10 is good, 4.11 would be better
    - Have to compile it from source ( already done )

## What would be needed to run HermitCore as a PV guest

If we want to run HermitCore as a 'classical' PV guest we have to make sure we have the following:

- A 64 Bit entrypoint
- Use to PV type Hypercalls for a lot of things
- Get paravirtualized MMU working
- Use paravirtualized interrupts
- Only use paravirtualized device drivers
- Use special PV console

Pros:
- Can be run on any type of hardware
- Faster than HVM

Cons:
- Have to rewrite a lot of functionalities including
    - memory management
    - interrupt handling
    - cpu initialization
    - power management
    - console output

