%macro elfnote 2-3 ""  ;name, type, descr

  align 4
  %strlen namesz %1
  %strlen descsz %3
  dd namesz
  dd descsz
  dd %2
  dd %1
  %if descsz > 0
    dd %3
  %endif

%endmacro
