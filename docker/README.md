# Docker build container
Use a docker conatiner to speed up your building process.
First, create an image from the Dockerfile
```
$ docker build -t rwthos/hermitcore:xen .
```
When it is done, just run
```
$ ./docker-start-container.sh
```
This will start a container and automagically build HermitCore for you. Afterwards you can start up a vagrant box and test some of the applications.
