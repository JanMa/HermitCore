#!/bin/bash
cd /HermitCore
rm -rf build
. cmake/local-cmake.sh
mkdir build
cd build
cmake ..
make
exit
