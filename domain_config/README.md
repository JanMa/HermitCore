# Domain configurations for Xen paravirtualized guests

In this folder you will find many prewritten domain configrations for HermitCore applications.
To use them do the following in your vagrant box:
```
$ cd domain_config
$ sudo xl create -c hello.conf
$
```
