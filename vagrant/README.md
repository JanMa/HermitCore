# HermitCore Vagrant Box

We provide a Vagrant Box to ease the use of the Xen Branches. It is based on the latest Ubuntu 18.04 and includes the Xen Hypervisor compiled from source with enabled debug options.

## Prerequesites

To use it, you need the follwing things installed on your machine
- vagrant (obviously)
- VirtualBox or LibVirt and VirtManager

It is recommended to use LibVirt as the paravirtprovider for vagrant.
For this to work you have to install the libvirt plugin.

```
$ vagrant plugin install vagrant-libvirt
```
If you run into problems please habe a look at the plugins [GitHub page](https://github.com/vagrant-libvirt/vagrant-libvirt).

## Usage
### LibVirt
Open a terminal and change into the `vagrant` folder. Then simply type
```
$ vagrant up --provider=libvirt
```
Ater a few Minutes you will be able to use `vagrant ssh` to login to the machine.
If you want to see console output when running HermitCore as a paravirtualized guest on Xen, you have to open `VirtManager` and use the included serial console. Alternatively you can use the `xl dmesg` command to see all output at once.

On creation, this Git repository will be copied to the VM. You will find it at `/HermitCore` with all the changes you might have made.

### VirtualBox
**It is recommended to use LibVirt since VirtualBox is not able to use nested virtualization. So you cannot start HVM guests in Xen!**

Open a terminal and change into the `vagrant` folder. Then simply type
```
$ vagrant up --provider=virtualbox
```
Ater a few Minutes you will be able to use `vagrant ssh` to login to the machine.
If you want to see console output when running HermitCore as a paravirtualized guest on Xen, open a second terminal window and navigate to the vagrant directory. Then use `tail -f xen.log` to follow the output. Alternatively you can use the `xl dmesg` command to see all output at once.

On creation, this Git repository will be copied to the VM. You will find it at `/HermitCore` with all the changes you might have made.
