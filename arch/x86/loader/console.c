#include <xen/console.h>
#include <xen/sched.h>
#include <xen/barrier.h>
#include <xen/event.h>
#include <stdio.h>
#include <string.h>

static evtchn_port_t console_evt;
extern const void kernel_start;
struct xencons_interface * console;

/* Write a NULL-terminated string */
int console_write(char * message)
{
	struct evtchn_send event;
	event.port = console_evt;
	int length = 0;
	while(*message != '\0')
	{
		/* Wait for the back end to clear enough space in the buffer */
		XENCONS_RING_IDX data;
		do
		{
			data = console->out_prod - console->out_cons;
			HYPERVISOR_event_channel_op(EVTCHNOP_send, &event);
			mb();
		} while (data >= sizeof(console->out));
		/* Copy the byte */
		int ring_index = MASK_XENCONS_IDX(console->out_prod, console->out);
		console->out[ring_index] = *message;
		/* Ensure that the data really is in the ring before continuing */
		wmb();
		/* Increment input and output pointers */
		console->out_prod++;
		length++;
		message++;
	}
	HYPERVISOR_event_channel_op(EVTCHNOP_send, &event);
	return length;
}


/* Block while data is in the out buffer */
void console_flush(void)
{
	/* While there is data in the out channel */
	while(console->out_cons < console->out_prod)
	{
		/* Let other processes run */
		HYPERVISOR_sched_op(SCHEDOP_yield, 0);
		mb();
	}
}


void print(int direct, const char *fmt, va_list args)
{
    static char   buf[1024];

    (void)xsnprintf(buf, sizeof(buf), fmt, args);

    if(direct)
    {
        (void)HYPERVISOR_console_io(CONSOLEIO_write, strlen(buf), buf);
        return;
    }else{
		(void)console_write(buf);
		if (buf[strlen(buf)-1] == '\n') {
			/* Feed a carriage return after a new line,
			 * otherwise we will just start somewhere in the line
			 */
			(void)console_write("\r");
		}
		return;
	}
}

void printk(const char *fmt, ...)
{
    va_list       args;
    va_start(args, fmt);
    print(0, fmt, args);
    va_end(args);
}

void xprintk(const char *fmt, ...)
{
    va_list       args;
    va_start(args, fmt);
    print(1, fmt, args);
    va_end(args);
}
