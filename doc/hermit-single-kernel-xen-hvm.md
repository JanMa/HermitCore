# Make HermitCore boot in Xen (Single Kernel HVM)

Xen includes two modes of operation. One is running paravirtualized guests and the other is hardware assisted virtualization. CPUs that support virtualization make it possible to run unmodified guests, including operation systems such as Microsoft Windows. In Xen this is called hardware virtual machine (HVM).  Xen uses device emulation based on QEMU to provide I/O virtualization to the virtual machines. This means the virtual machines see an emulated version of a fairly basic PC.

Because the virtualization of HVM guests is based on QEMU, we should be able (in theory) to run HermitCore unmodifed as a guest in Xen. When booting HermitCore via QEMU directly (as done in the tutorial), the kernel is booted via  the GRUB Multiboot protocol. Unfortunately at the time of writing Xen doesn't support this (yet?). So it will not be possible to boot our kernel directly. We have to take a small detour by starting a GRUB bootloader and then booting into HermitCore.

## Create a bootable GRUB ISO

The easiest way to create a bootable media is to use the `grub-mkrescure` tool. It is included in every GRUB installation on major Linux distributions. GRUB modules are also needed. On Ubuntu they are included in the package `grub-pc-bin`. To create a GRUB rescue disk, we first have to create the folder structure of our new ISO, containing the GRUB config file, the HermitCore loader and the application we want to run. This should look like this

```
└── iso
    └── boot
        ├── grub
        │   └── grub.cfg
        ├── hermit.application
        └── ldhermit.elf
```   

The `grub.cfg` file should look like this

```
default=0
timeout=0

menuentry "HermitCore" {
	multiboot /boot/ldhermit.elf
	module /boot/hermit.application
	boot
}

```

It tells GRUB to boot the HermitCore loader via Multiboot and loads the HermitCore application as a module. If you want to pass command line options to the kernel, you can do it after the `multiboot` statement. For example:

```
multiboot /boot/ldhermit.elf -uart=io:0x3f8
```

This enables output on the serial port COM1.

To create the ISO file, we invoke `grub-mkrescue` like this

```
grub-mkrescue -o hermit.iso iso/
```

This includes the folder structure we just created and outputs the `hermit.iso` file. You could now boot this ISO via QEMU and it would work fine. If we want to boot it via Xen we still need to create a domain configuration file.

## Configure a HVM domain

Our domain config file should look like this:

```
# This configures an HVM rather than PV guest
builder = "hvm"

# Guest name
name = "hermit-single.hvm"

# Initial memory allocation (MB)
memory = 1024

# Number of VCPUS
vcpus = 1

# Network devices
vif = [ 'model=rtl8139,bridge=br0' ]

# Disk Devices
disk = [ 'file:/tmp/hermit.iso,hdc:cdrom,r' ]

#Disable VGA output
nographic = 1

#Serial Console Output
serial = [ 'file:/tmp/hermit.log' ]

tsc_mode="native"

```

This is pretty straightfoward. We tell Xen to create a HVM guest by setting `builder=hvm`, give it a name, some memory and one virtual CPU. HermitCore supports the `rtl8139` chipset for network devices, so we add a device using this chipset and bridge it to our bridge `br0` (The name of the device might be different on other Linux distributions). We insert the ISO we just created as a CD and disable graphic output. To see kernel boot messages, we attach a serial port and write the output to a temporary log file. While our guest is running, we can use `tail -f` to watch it boot. The last line is important, since it tells Xen how to emulate the TSC of our guest.

```
tsc_mode="MODE"
(x86 only) Specifies how the TSC (Time Stamp Counter) should be provided to the guest. Specifying this option as a number is deprecated.

Options are:

default
Guest rdtsc/p is executed natively when monotonicity can be guaranteed and emulated otherwise (with frequency scaled if necessary).

If a HVM container in default TSC mode is created on a host that provides constant host TSC, its guest TSC frequency will be the same as the host. If it is later migrated to another host that provide constant host TSC and supports Intel VMX TSC scaling/AMD SVM TSC ratio, its guest TSC frequency will be the same before and after migration, and guest rdtsc/p will be executed natively after migration as well

always_emulate
Guest rdtsc/p is always emulated and the virtual TSC will appear to increment (kernel and user) at a fixed 1GHz rate, regardless of the pCPU HZ rate or power state. Although there is an overhead associated with emulation, this will NOT affect underlying CPU performance.

native
Guest rdtsc/p is always executed natively (no monotonicity/frequency guarantees). Guest rdtsc/p is emulated at native frequency if unsupported by h/w, else executed natively.

native_paravirt
Same as native, except Xen manages the TSC_AUX register so the guest can determine when a restore/migration has occurred and assumes guest obtains/uses a pvclock-like mechanism to adjust for monotonicity and frequency changes.

If a HVM container in native_paravirt TSC mode can execute both guest rdtsc and guest rdtscp natively, then the guest TSC frequency will be determined in a similar way to that of default TSC mode.
```

We need to use `native` mode, otherwise the time measures are completely wrong.

## Start the domain

To start the domain, we simply have to invoke `xl` with the following arguments

```
xl create /path/to/domain_config
```
where `domain_config` is the configuration file we just created. Make sure you used the right paths for the ISO and the log file!

## Put everything together

This branch of HermitCore includes a wrapper script which does all the things we just did by hand. After you built HermitCore, you can find it in the following directory
```
HermitCore/build/local_prefix/opt/hermit/tools/xen-single-kernel.sh
```
It can be used as follows:

```
Usage: command -hvlknmc args

This script starts a HermitCore application as a single kernel HVM Domain in Xen.
Make sure to destroy your domain when you are finished!

Args:
  -l  Path to hermit loader
  -k  Path to hermit application
  -n  Only create files, do not start
  -m  Domain memory in MB (Default 1024)
  -c  Number of CPU cores (Default 1)
  -v  print VERSION
  -h  this help screen
```

![Xen demo](image/demo.gif)

## Conclusion

So far this is working pretty well. The only things not working at the moment are multiple CPUS (HermitCore fails to start additional CPUS, but this might also be a problem originating from running Xen inside a vm) and the shutdown of a domain.
Also boot times are pretty bad compared to starting HermitCore via QEMU or uhyve. It takes about 5 seconds to boot the simple hello world test in Xen whereas booting in QEMU takes about 170 ms. Hopefully this will speed up once we add paravirtualization support to HermitCore.
